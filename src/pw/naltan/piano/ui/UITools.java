package pw.naltan.piano.ui;

import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;

/**
 * This class contains some simple JavaFX shortcuts for doing things like adding
 * tooltips and adding to the main GridPane without repeating myself or using
 * several lines for something simple.
 * 
 * @author Nate Stowwe
 *
 */
public class UITools {

	/**
	 * Add a tooltip to a handful of nodes.
	 * 
	 * @param hint - The string to use as the tooltip.
	 * @param n    - The nodes to add the tooltip to.
	 */
	public static void addTooltip(String hint, Node... n) {
		Tooltip t = new Tooltip(hint);
		for (Node current : n) {
			Tooltip.install(current, t);
		}
	}

	/**
	 * Add a node to a GridPane at the specified coordinates
	 * 
	 * @param p - The pane to add it to.
	 * @param n - The node to add.
	 * @param r - The row to add it to.
	 * @param c - The column to add it to.
	 */
	public static void addToGP(GridPane p, Node n, int r, int c) {
		GridPane.setRowIndex(n, r);
		GridPane.setColumnIndex(n, c);
		p.getChildren().add(n);
	}
}
