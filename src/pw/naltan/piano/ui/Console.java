package pw.naltan.piano.ui;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TextArea;

/**
 * This class contains code for the printstream for the JavaFX console.
 * 
 * @author Nate Stowwe
 *
 */
public class Console extends OutputStream {

	private TextArea output;
	private List<String> prompts;

	// This is the cutoff before the scrollbar appears.
	private static final int CUTOFF = 17;
	// The character code for newline.
	private static final int newline = 0x0a;

	/**
	 * Initialize the console in a TextArea
	 * 
	 * @param ta - The TextArea to use as the console.
	 */
	public Console(TextArea ta) {
		this.output = ta;
		prompts = new ArrayList<>();
		for (int i = 0; i < CUTOFF; i++) {
			prompts.add("");
		}
	}

	@Override
	/**
	 * Writes the charcode provided to the console.
	 */
	public void write(int i) throws IOException {

		if (i == newline) {
			prompts.add(0, "");
			prompts.remove(CUTOFF);
		} else {
			prompts.set(0, prompts.get(0) + String.valueOf((char) i));
		}

		String out = "";
		for (String current : prompts) {
			out = current + "\n" + out;
		}
		output.setText(out.substring(0, out.length() - 2));
	}
}