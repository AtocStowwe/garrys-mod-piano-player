package pw.naltan.piano;

//TODO: In the blurb

/**
 * This class contains some publicly available preferences that other parts of
 * the program can access. In the future, this should be made into an object, so
 * that other classes can't illegally modify the values here.
 * 
 * @author Nate Stowwe
 *
 */
public class Preferences {
	// The server's tickrate.
	public static int TICKRATE = 60;
	// Whether the program should try to shift octaves that are out of bounds.
	public static boolean SHIFT_OCTAVES = false;
	// Whether the program should output to the JavaFX console.
	public static boolean USE_CONSOLE = false;
	// Whether the program should be playing or not.
	public static boolean STARTED = false;
}
