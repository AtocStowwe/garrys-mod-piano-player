package pw.naltan.piano;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;

/**
 * This class extends the Midi Receiver class, and is specially designed to
 * convert midi note commands into keybord keypresses.
 * 
 * @author Nate Stowwe
 *
 */
public class MidiInputReceiver implements Receiver {

	// Does nothing and is just required for the implementation.
	public MidiInputReceiver() {
	}

	// Does nothing and is just required for the implementation.
	public void close() {
	}

	/**
	 * Executes whenever a key is pressed.
	 */
	public void send(MidiMessage msg, long timeStamp) {
		// m0 = the command
		// m1 = the note to play
		// m2 = the intensity, or 0 if note off.

		byte[] m = msg.getMessage();

		// If we've not started the player or the command length isn't what we want,
		// stop.
		if (!Preferences.STARTED || m.length != 3) {
			return;
		}

		// If we're out of range and not shifting the octaves, stop.
		if (!Preferences.SHIFT_OCTAVES && (m[1] < 36 || m[1] > 96)) {
			return;
		}

		/// If we pass those two checks, we're golden.
		if (m[2] != 0 && m[0] == -112) {
			if (Preferences.USE_CONSOLE) {
				System.out.println(Player.keys[m[1]].toString());
			}
			new KeyPlayer(m[1]).start();
		}
	}

}